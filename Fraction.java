
public class Fraction {
	private int numerateur;
	private int denominateur;
	
	Fraction ( int numerateur, int denominateur)
	{
		this.setNumerateur(numerateur);
		this.setDenominateur(denominateur);
	}	
	Fraction (int numerateur)
	{
		this.setNumerateur(numerateur);
		this.setDenominateur(1);
	}
	
	Fraction()
	{
		this.setNumerateur(0);
		this.setDenominateur(1);
	}
	public int getNumerateur() {
		return numerateur;
	}
	public void setNumerateur(int numerateur) {
		this.numerateur = numerateur;
	}
	public int getDenominateur() {
		return denominateur;
	}
	public void setDenominateur(int denominateur) {
		this.denominateur = denominateur;
	}
	public String toString(){
		return this.numerateur +"/"+ this.denominateur;
		
	}
	public static final Fraction UN = new Fraction (1,1) ;
	public static final Fraction ZERO = new Fraction (1,0);

	public double getdouble ( )
	{
		return ( this.getNumerateur()/this.getDenominateur())
	}
	public bool compare_est_plus_grand(Fraction f)
	{
		if(this.getdouble()>f.getdouble())
		{
			return true;
		}else{
			return false ;
		}
	}
	public bool compare_est_plus_petit(Fraction f)
	{
		if(this.getdouble()<f.getdouble())
		{
			return true;
		}else{
			return false ;
		}
	}
}

